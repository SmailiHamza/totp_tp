package totpapp;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import totpapp.service.LoginService;
import totpapp.service.RegisterService;
import totpapp.service.TotpGenerator;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ActiveProfiles("test")
class SpringEndToEndTest {

    @Autowired
    RegisterService registerService;
    @Autowired
    TotpGenerator totpGenerator;
    @Autowired
    LoginService loginService;

    @Test
    void signUpAndSignInTest() {
        long key = registerService.register("a@b.c");
        long code=totpGenerator.getCode("a@b.c");
        assertTrue(loginService.attemptLogin("a@b.c", code));
    }
    @Test
    void signUpAndSignInWithWrongCodeTest() {
        long key = registerService.register("aa@b.c");
        assertFalse(loginService.attemptLogin("aa@b.c", 000000));
    }
    @Test
    void signUpAndSignInWithDelayTest() throws InterruptedException {
        long key = registerService.register("aaa@b.c");
        long code=totpGenerator.getCode("aaa@b.c");

        Thread.sleep(4000);
        assertFalse(loginService.attemptLogin("aaa@b.c", code));
    }

}
