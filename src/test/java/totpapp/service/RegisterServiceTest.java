package totpapp.service;

import totpapp.models.User;
import totpapp.data.UserRepository;
import org.junit.jupiter.api.Test;
import totpapp.service.RandomKeyGenerator;
import totpapp.service.RegisterService;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class RegisterServiceTest {
    @Test
    public void registerTest(){
        RandomKeyGenerator randomKeyGenerator=new RandomKeyGenerator(new FakeRandom());
        MockUserRepository userRepository=new MockUserRepository();
        RegisterService registerService=new RegisterService(randomKeyGenerator ,userRepository);
        assertEquals(1000,registerService.register("ha@mail.fr"));
        assertTrue(userRepository.isAddedUser());
    }

    class FakeRandom extends Random{
        public long nextLong(){
            return 1000;
        }
    }
    class MockUserRepository extends UserRepository{
         boolean addedUser=false;
        public  void save(User user){
            if(user.getKey()==1000 && user.getEmail()=="ha@mail.fr")
            addedUser=true;
        }
        public boolean isAddedUser() {
            return addedUser;
        }
    }
}
