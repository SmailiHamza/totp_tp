package totpapp.service;

import totpapp.data.UserRepository;
import org.junit.jupiter.api.Test;
import totpapp.service.LoginService;
import totpapp.service.RandomKeyGenerator;
import totpapp.service.RegisterService;
import totpapp.service.TotpGenerator;

import java.time.*;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LoginServiceTest {
    @Test
    public void attemptLoginTest() {
        UserRepository userRepository = new UserRepository();
        RandomKeyGenerator randomKeyGenerator = new RandomKeyGenerator(new Random());
        RegisterService registerService = new RegisterService(randomKeyGenerator, userRepository);
        long key = registerService.register("a@b.c");
        TotpGenerator totpGenerator = new TotpGenerator(Clock.systemDefaultZone(),30,userRepository);
        LoginService loginService = new LoginService(userRepository, totpGenerator);
        long code = totpGenerator.getCode("a@b.c");
        assertTrue(loginService.attemptLogin("a@b.c", code));
    }

    @Test
    public void attemptLoginTestWithDelay() {
        UserRepository userRepository = new UserRepository();
        RandomKeyGenerator randomKeyGenerator = new RandomKeyGenerator(new Random());
        RegisterService registerService = new RegisterService(randomKeyGenerator, userRepository);
        long key = registerService.register("a@b.c");
        LocalDateTime dateTime = LocalDateTime.of(2020, 5, 24, 14, 0);
        Instant instant = ZonedDateTime.of(dateTime, ZoneId.systemDefault()).toInstant();
        Clock clock = Clock.fixed(instant, ZoneId.systemDefault());
        TotpGenerator totpGenerator = new TotpGenerator(clock,30,userRepository);
        LocalDateTime dateTimeAfterTwoMinute = LocalDateTime.of(2020, 5, 24, 14, 2);
        Instant instantTimeAfterTwoMinute = ZonedDateTime.of(dateTimeAfterTwoMinute, ZoneId.systemDefault()).toInstant();
        Clock clockTimeAfterTwoMinute = Clock.fixed(instantTimeAfterTwoMinute, ZoneId.systemDefault());
        TotpGenerator totpGeneratorTimeAfterTwoMinute = new TotpGenerator(clockTimeAfterTwoMinute,30,userRepository);
        LoginService loginService = new LoginService(userRepository, totpGeneratorTimeAfterTwoMinute);
        long code = totpGenerator.getCode("a@b.c");
        assertFalse(loginService.attemptLogin("a@b.c", code));
    }
}
