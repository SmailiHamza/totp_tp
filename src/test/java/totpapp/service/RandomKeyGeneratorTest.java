package totpapp.service;

import org.junit.jupiter.api.Test;
import totpapp.service.RandomKeyGenerator;

import static org.junit.jupiter.api.Assertions.*;
import java.util.Random;

public class RandomKeyGeneratorTest {
    @Test
    public void generateKeyTest(){
        Random r=new Random(0);
        RandomKeyGenerator randomKeyGenerator=new RandomKeyGenerator(new Random(0));
        assertEquals(Math.abs(r.nextLong()),randomKeyGenerator.generateKey());
    }
    @Test
    public void generateKeyWithFakeRandomTest(){
        RandomKeyGenerator randomKeyGenerator=new RandomKeyGenerator(new FakeRandom());
        assertEquals(1000,randomKeyGenerator.generateKey());
    }
    class FakeRandom extends Random{
        public long nextLong(){
            return 1000;
        }
    }
}
