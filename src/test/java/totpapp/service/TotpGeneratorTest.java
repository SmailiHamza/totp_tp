package totpapp.service;

import org.junit.jupiter.api.Test;
import totpapp.data.UserRepository;
import totpapp.service.TotpGenerator;

import java.time.*;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class TotpGeneratorTest {
    UserRepository userRepository = new UserRepository();
    RandomKeyGenerator randomKeyGenerator = new RandomKeyGenerator(new Random());
    @Test
    public void getCodeTest() {

        RegisterService registerService = new RegisterService(randomKeyGenerator, userRepository);
        long key = registerService.register("a@b.c");
        TotpGenerator totpGenerator = new TotpGenerator(Clock.systemDefaultZone(),30,userRepository);
        assertEquals(totpGenerator.getCode("a@b.c")
                , totpGenerator.getCode("a@b.c"));
    }

    @Test
    public void getCodeTestWithDelay() {
        RegisterService registerService = new RegisterService(randomKeyGenerator, userRepository);
        long key = registerService.register("a@b.c");
        LocalDateTime dateTime = LocalDateTime.of(2020, 5, 24, 14, 0);
        Instant instant = ZonedDateTime.of(dateTime, ZoneId.systemDefault()).toInstant();
        Clock clock = Clock.fixed(instant, ZoneId.systemDefault());
        LocalDateTime dateTimeAfterTwoMinute = LocalDateTime.of(2020, 5, 24, 14, 2);
        Instant instantTimeAfterTwoMinute = ZonedDateTime.of(dateTimeAfterTwoMinute, ZoneId.systemDefault()).toInstant();
        Clock clockTimeAfterTwoMinute = Clock.fixed(instantTimeAfterTwoMinute, ZoneId.systemDefault());
        TotpGenerator totpGenerator = new TotpGenerator(clock,30,userRepository);
        TotpGenerator totpGeneratorTimeAfterTwoMinute = new TotpGenerator(clockTimeAfterTwoMinute,30,userRepository);
        assertNotEquals(totpGenerator.getCode("a@b.c")
                , totpGeneratorTimeAfterTwoMinute.getCode("a@b.c"));
    }
}
