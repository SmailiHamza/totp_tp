package totpapp;

import org.springframework.context.annotation.Profile;
import totpapp.data.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import totpapp.service.LoginService;
import totpapp.service.RegisterService;
import totpapp.service.TotpGenerator;

import java.util.Scanner;

import static java.lang.Long.parseLong;

@Component
@Profile("!test")
public class SpringRun implements CommandLineRunner {
    UserRepository userRepository;
    RegisterService registerService;
    TotpGenerator totpGenerator;
    LoginService loginService;
    Scanner scanner;

    @Autowired
    public SpringRun(UserRepository ur, RegisterService rs, TotpGenerator totp, LoginService ls, Scanner sc) {
        this.userRepository = ur;
        this.registerService = rs;
        this.totpGenerator = totp;
        this.loginService = ls;
        this.scanner = sc;
    }

    @Override
    public void run(String... args) throws Exception {
        String email;
        System.out.println(" _________________________________\n|------------  Sign Up  ----------|");
        System.out.println("Email or Stop:");
        while (!((email = scanner.next()).equalsIgnoreCase("stop"))) {
            long key = registerService.register(email);
            System.out.println(key);
            System.out.println("Email or Stop:");
        }
        boolean stop = false;
        System.out.println("|------------  Sign In  ----------|");
        while (!stop) {
            System.out.println("Saissez vorte mail ");
            email = scanner.next();

            System.out.println(totpGenerator.getCode(email));
            System.out.println("Saissez vorte code ");

            long code = parseLong(scanner.next());

            if (this.loginService.attemptLogin(email, code)) {
                System.out.println("Connected !");
                stop = true;
            } else {
                System.out.println("Wrong code !");
            }
        }
    }
}

