package totpapp.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Clock;
import java.util.Random;
import java.util.Scanner;

@Configuration
public class BeanPrinterConfig {
    @Bean
    public Random random(){
        return new Random();
    }
    @Bean
    public Clock clock(){
        return Clock.systemDefaultZone();
    }
    @Bean
    public Scanner scanner(){
        return new Scanner(System.in);
    }

}
