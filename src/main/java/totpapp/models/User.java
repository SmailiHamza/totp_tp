package totpapp.models;

public class User {
    private String email;
    private long key;

    public User(String email, long key) {
        this.email = email;
        this.key = key;
    }

    public long getKey() {
        return key;
    }

    public String getEmail() {
        return email;
    }
}
