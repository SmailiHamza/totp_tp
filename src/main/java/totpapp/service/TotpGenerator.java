package totpapp.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import totpapp.data.UserRepository;

import java.time.Clock;
import java.time.Instant;

@Component
public class TotpGenerator {
    Clock clock;
    int delay;
    UserRepository userRepository;
    public TotpGenerator(Clock clock,@Value("${totp-app.delay-pwd}") int delay,UserRepository userRepository) {
        this.clock = clock;
        this.delay=delay;
        this.userRepository=userRepository;
    }

    public long getCode(String email) {
        long key=userRepository.getListUser().get(email).getKey();
        long getInstant = Instant.now(clock).getEpochSecond() / delay;
        long code = (key + getInstant) % 1000000;
        return (code);
    }
}
