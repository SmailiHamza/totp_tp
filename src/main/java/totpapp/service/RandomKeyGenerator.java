package totpapp.service;

import org.springframework.stereotype.Component;

import java.util.Random;
@Component
public class RandomKeyGenerator {
    Random random;

    public RandomKeyGenerator(Random r) {
        random = r;
    }

    public long generateKey() {
        long key = random.nextLong();
        return Math.abs(key);
    }
}
