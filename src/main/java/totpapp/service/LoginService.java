package totpapp.service;


import totpapp.data.UserRepository;
import org.springframework.stereotype.Component;

@Component
public class LoginService {
    TotpGenerator totpGenerator;
    UserRepository userRepository;

    public LoginService(UserRepository userRepository,TotpGenerator totpGenerator) {
        this.totpGenerator = totpGenerator;
        this.userRepository = userRepository;
    }

    public boolean attemptLogin(String email, long code) {
        long genCode = 0;
        if (userRepository.getListUser().containsKey(email)) {
            genCode = totpGenerator.getCode(email);
        } else {
            System.out.println("L'adresse email n'existe pas !");
        }
        if (code == genCode) {
            return true;
        } else {
            return false;
        }
    }
}
