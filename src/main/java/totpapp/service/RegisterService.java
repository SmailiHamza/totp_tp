package totpapp.service;



import totpapp.data.UserRepository;
import totpapp.models.User;
import org.springframework.stereotype.Component;

@Component
public class RegisterService {
    RandomKeyGenerator randomKeyGenerator;
    UserRepository userRepository;

    public RegisterService(RandomKeyGenerator randomKeyGenerator, UserRepository userRepository) {
        this.randomKeyGenerator = randomKeyGenerator;
        this.userRepository = userRepository;
    }

    public long register(String email) {
        User user;
        long key = randomKeyGenerator.generateKey();
        user = new User(email, key);
        userRepository.save(user);
        return key;
    }

}
