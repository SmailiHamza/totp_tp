package totpapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringTotpApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringTotpApplication.class, args);
    }

}
