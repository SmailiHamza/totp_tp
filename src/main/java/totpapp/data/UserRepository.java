package totpapp.data;



import totpapp.models.User;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class UserRepository {
    private HashMap<String, User> listUser;

    public UserRepository() {
        listUser = new HashMap<>();
    }

    public HashMap<String, User> getListUser() {
        return listUser;
    }

    public void save(User user) {
        if (listUser.containsKey(user.getEmail())) {
            throw new RuntimeException("Adresse e-mail existe déja !");
        }
        listUser.put(user.getEmail(), user);
    }
}
